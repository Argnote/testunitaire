<?php

namespace App\Controller;

use App\Entity\Item;
use App\Entity\TodoList;
use App\Form\ItemType;
use App\Form\TodoListType;
use App\Repository\TodoListRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TodoListController extends AbstractController
{

    /**
     * @Rest\Route("/Todolist", name="getTodolist", methods={"GET"})
     * */
    public function getTodolist(Request $request, EntityManagerInterface $entityManager, TodoListRepository $listRepository)
    {
        $todoList = $listRepository->findOneBy(["id"=>$request->query->get('id')]);

        if(empty($todoList))
            return new Response("Aucune todolist trouvé!!", 404);
        return new Response("La todolist existe", 200);

    }

    /**
     * @Rest\Route("/ajout-item", name="addItem", methods={"POST"})
     * */
    public function addItem(Request $request, EntityManagerInterface $entityManager, TodoListRepository $listRepository)
    {
        $todo = $listRepository->findOneBy(["id"=>$request->request->get('todoListID')]);
        if(empty($todo))
            return new Response("Aucune todolist trouvé!!", 404);

        $item = new Item();
        $item->setContent($request->request->get('content'));
        $item->setName($request->request->get('name'));
        $item->setCreationDate(new \DateTime($request->request->get('creationDate')));

        $todo->addItem($item);

        return new Response("Item ajouté", 201);
    }

    /**
     * @Rest\Route("/nouvelle-todolist", name="addTodolist", methods={"POST"})
     * */
    public function newTodolist(Request $request, EntityManagerInterface $entityManager, UserRepository $userRepository)
    {
        $user = $userRepository->findOneBy(["id"=>$request->request->get('userId')]);
        if(empty($user))
            return new Response("Aucun utilisateur trouvé!!", 404);

        $todoList = new TodoList();
        $todoList->setName($request->request->get('name'));
        $todoList->setDescription($request->request->get('description'));

        $user->setTodoList($todoList);

        return new Response("TodoList ajouté", 201);
    }

    /**
     * @Rest\Route("/remove-todolist", name="removeTodolist", methods={"DELETE"})
     * */
    public function deleteTodolist(Request $request, EntityManagerInterface $entityManager, TodoListRepository $listRepository)
    {
        $todoList = $listRepository->findOneBy(["id"=>$request->query->get('todoListId')]);
        if(empty($todoList))
            return new Response("Aucune todoListTrouvé!!", 404);

        if ($todoList->getUserID()->getId() != $request->query->get('userId'))
            return new Response("La todoList n'appartient pas a cet utilisateur!!", 500);

        return new Response("La todoList à été supprimé", 200);
    }


}

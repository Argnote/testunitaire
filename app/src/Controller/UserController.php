<?php
/**
 * Created by PhpStorm.
 * User: valen
 * Date: 11/02/2021
 * Time: 18:07
 */

namespace App\Controller;


use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;

class UserController extends AbstractController
{
    /**
     * @Rest\Route("/register", methods={"POST"})
     */
    public function register(Request $request, UserRepository $userRepository)
    {
        $user = new User();
        $user->setLastname($request->request->get('lastname'));
        $user->setFirstname($request->request->get('firstname'));
        $user->setEmail($request->request->get('email'));
        $user->setBirthday(new \DateTime($request->request->get('birthday')));
        $user->setPassword($request->request->get('password'));

        if ($userRepository->findOneBy(["email" => $user->getEmail()])) {
            return new Response("L'utilisateur existe déjà", 500);
        }

        $user->isValid();

        return new Response("L'utilisateur a été créé", 201);

    }


}
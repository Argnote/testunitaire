<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Carbon\Carbon;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastname;

    /**
     * @ORM\Column(type="date")
     */
    private $birthday;

    /**
     * @ORM\OneToOne(targetEntity=TodoList::class, mappedBy="userID", cascade={"persist", "remove"})
     */
    private $todoList;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getBirthday(): ?\DateTimeInterface
    {
        return $this->birthday;
    }

    public function setBirthday(\DateTimeInterface $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getTodoList(): ?TodoList
    {
        return $this->todoList;
    }

    public function setTodoList(?TodoList $todoList): self
    {
        if (!empty($this->getTodoList())) throw new \Exception("L'utilisateur ne peut avoir qu'une seule todolist");
        if ($this->isValid() === false) throw new \Exception("Le user n'est pas valide");

        $this->todoList = $todoList;

        // set (or unset) the owning side of the relation if necessary
        $newUserID = null === $todoList ? null : $this;
        if ($todoList->getUserID() !== $newUserID) {
            $todoList->setUserID($newUserID);
        }

        return $this;
    }

    public function isValid() {

        if (empty($this->firstname))
            throw new \Exception("Il manque le prénom");

        if (empty($this->lastname))
            throw new \Exception("Il manque le nom");

        if (empty($this->email))
            throw new \Exception("Il manque l'email");

        if (!filter_var($this->email, FILTER_VALIDATE_EMAIL))
            throw new \Exception("L'email n'est pas valide");

        if (empty($this->birthday))
            throw new \Exception("Il manque l'anniversaire");

        if (empty($this->password))
            throw new \Exception("Il manque le mot de passe");

        if (strlen($this->password) <= 8)
            throw new \Exception("Le mot de passe est trop court");

        if (strlen($this->password) >= 40)
            throw new \Exception("Le mot de passe est trop court");

        if (Carbon::now()->subYears(13)->isBefore($this->birthday))
            throw new \Exception("L'utilisateur est trop jeune");

    }
}

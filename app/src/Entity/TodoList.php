<?php

namespace App\Entity;

use App\Repository\TodoListRepository;
use Carbon\Carbon;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TodoListRepository::class)
 */
class TodoList
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToOne(targetEntity=User::class, inversedBy="todoList", cascade={"persist", "remove"})
     */
    private $userID;

    /**
     * @ORM\OneToMany(targetEntity=Item::class, mappedBy="todoListID")
     */
    private $items;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getUserID(): ?User
    {
        return $this->userID;
    }

    public function setUserID(?User $userID): self
    {
        $this->userID = $userID;

        return $this;
    }

    /**
     * @return Collection|Item[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(Item $item): self
    {
        if(count($this->items)==10)
            throw new \Exception("Vous ne pouvez pas avoir plus de 10 items dans votre todoList");

        if(strlen($item->getContent()) > 1000)
            throw new \Exception("le nombre de caractère du contenu est trop élevé");

        if(empty($item->getName()))
            throw new \Exception("Il faut un nom");

        if(empty($item->getCreationDate()))
            throw new \Exception("Une erreur est survenu avec la date");

        foreach ($this->items as $value)
        {
            if($item->getName() == $value->getName())
                throw new \Exception("Ce nom est déja utilisé");

            if( $item->getCreationDate()->getTimestamp() - $value->getCreationDate()->getTimestamp() < 1800)
            {
                throw new \Exception("Un item à été crée il y a moins de 30 minutes");
            }
        }

        if (!$this->items->contains($item)) {
            $this->items[] = $item;
            $item->setTodoListID($this);
        }
        return $this;
    }

    public function removeItem(Item $item): self
    {
        if ($this->items->contains($item)) {
            $this->items->removeElement($item);
            // set the owning side to null (unless already changed)
            if ($item->getTodoListID() === $this) {
                $item->setTodoListID(null);
            }
        }

        return $this;
    }
}

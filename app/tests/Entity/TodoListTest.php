<?php

namespace App\Tests\Entity;

use App\Entity\Item;
use App\Entity\TodoList;
use PHPUnit\Framework\TestCase;

class TodoListTest extends TestCase
{
    protected $item;
    protected $todoList;

    protected function setUp():void
    {
        $this->item = new Item();
        $this->item->setContent("toto");
        $this->item->setCreationDate(new \DateTime('2020-10-29 21:30'));
        $this->item->setName("item test");

        $this->todoList = new TodoList();
        $this->todoList->setName("todoList");
        $this->todoList->setDescription("desc");

        parent::setUp();
    }

    public function testAddItemValidFirstItem()
    {
        $this->assertSame($this->todoList, $this->todoList->addItem($this->item));
    }

    public function testAddItemValidTwoItem()
    {
        $this->todoList->addItem($this->item);
        $item = new Item();
        $item->setContent("titi");
        $item->setCreationDate(new \DateTime('2020-10-30 21:30'));
        $item->setName("item test 2");

        $this->assertSame($this->todoList, $this->todoList->addItem($item));
    }

    public function testAddItemDoubleNameItem()
    {
        $this->todoList->addItem($this->item);

        $item = new Item();
        $item->setContent("toto");
        $item->setCreationDate(new \DateTime('2020-10-12 21:30'));
        $item->setName("item test");
        $this->expectException('Exception');
        $this->todoList->addItem($item);
    }

    public function testAddItemNoName()
    {
        $this->todoList->addItem($this->item);

        $item = new Item();
        $item->setContent("toto");
        $item->setCreationDate(new \DateTime('2020-10-11 21:45'));

        $this->expectException('Exception');
        $this->todoList->addItem($item);
    }

    public function testAddItemTimeless30()
    {
        $this->todoList->addItem($this->item);

        $item = new Item();
        $item->setContent("toto");
        $item->setCreationDate(new \DateTime('2020-10-11 21:45'));
        $item->setName("item");

        $this->expectException('Exception');
        $this->todoList->addItem($item);
    }

    public function testAddItemContentMore1000()
    {
        $this->todoList->addItem($this->item);

        $item = new Item();
        $item->setContent(str_repeat("a",1001));
        $item->setCreationDate(new \DateTime('2020-10-11 23:45'));
        $item->setName("item");

        $this->expectException('Exception');
        $this->todoList->addItem($item);
    }

    public function testAddItemInFullTodoList()
    {
        for($i = 1; $i<=10; $i++)
        {
            $item = new Item();
            $date = new \DateTime('2020-10-10 23:45');
            $date->modify('+'.$i.' day');
            $item->setName($i);
            $item->setContent($i);
            $item->setCreationDate($date);
            $this->todoList->addItem($item);
            unset($item);
        }

        $this->expectException('Exception');
        $this->todoList->addItem($this->item);

    }
}

<?php

namespace App\Tests\Entity;

use App\Entity\TodoList;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use \DateTime;

class UserTest extends WebTestCase
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function setUp() : void
    {
        static::bootKernel();
        $this->em = self::$kernel->getContainer()->get('doctrine.orm.entity_manager');
    }
    private $user;

    public function init() {
        $this->user = new User();
        $this->user->setEmail('valentin.roussel13@gmail.com');
        $this->user->setPassword('azerty123');
        $this->user->setFirstname('Valentin');
        $this->user->setLastname('Roussel');
        $this->user->setBirthday(new DateTime('1997-05-15'));
    }

    public function testNoEmail() {
        $this->init();
        $this->user->setEmail('');
        $this->expectException('Exception');
        $this->user->isValid();
    }

    public function testNoAtEmail() {
        $this->init();
        $this->user->setEmail('emailwithoutat.com');
        $this->expectException('Exception');
        $this->user->isValid();
    }

    public function testNoFirstname() {
        $this->init();
        $this->user->setFirstname('');
        $this->expectException('Exception');
        $this->user->isValid();
    }

    public function testNoLastname() {
        $this->init();
        $this->user->setLastname('');
        $this->expectException('Exception');
        $this->user->isValid();
    }

    public function testNoPassword() {
        $this->init();
        $this->user->setPassword('');
        $this->expectException('Exception');
        $this->user->isValid();
    }

    public function testShortPassword() {
        $this->init();
        $this->user->setPassword('aze');
        $this->expectException('Exception');
        $this->user->isValid();
    }

    public function testLongPassword() {
        $this->init();
        $pass = str_repeat('azerty123', 5);
        $this->user->setPassword($pass);
        $this->expectException('Exception');
        $this->user->isValid();
    }

    public function testNoBirthday() {
        $this->init();
        $this->user->setBirthday(new DateTime(''));
        $this->expectException('Exception');
        $this->user->isValid();
    }

    public function testBirthdayYoung() {
        $this->init();
        $this->user->setBirthday(new DateTime('2015-03-06'));
        $this->expectException('Exception');
        $this->user->isValid();
    }

    public function testUserNotValid() {
        $this->init();
        $this->user->setLastname('');
        $todolist = new TodoList();
        $this->expectException('Exception');
        $this->user->setTodoList($todolist);
    }

    public function testTwoTodoList() {
        $this->init();
        $this->user->setLastname('');
        $todolist1 = new TodoList();
        $todolist2 = new TodoList();
        $this->expectException('Exception');
        $this->user->setTodoList($todolist1);
        $this->user->setTodoList($todolist2);
    }
}

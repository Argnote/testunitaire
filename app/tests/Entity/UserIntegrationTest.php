<?php
///**
// * Created by PhpStorm.
// * User: valen
// * Date: 11/02/2021
// * Time: 12:57
// */
//
//namespace App\Tests\Entity;
//
//use Doctrine\ORM\EntityManagerInterface;
//use GuzzleHttp\Client;
//use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
//
//class UserIntegrationTest extends WebTestCase
//{
//    /**
//     * @var EntityManagerInterface
//     */
//    private $em;
//
//    protected static $url = "192.168.1.14:8082";
//
//    public function setUp() : void
//    {
//        static::bootKernel();
//        $this->em = self::$kernel->getContainer()->get('doctrine.orm.entity_manager');
//    }
//    private $user;
//
//    public function init() {
//        $user = [
//            "email" => 'valentin.roussel13@gmail.com',
//            "password" => 'azerty123',
//            "firstname" => 'Valentin',
//            "lastname" => 'Roussel',
//            "birthday" => new \DateTime('1997-05-15'),
//        ];
//
//        $client = new Client();
//        $response = $client->request("POST", self::$url."/login", json_encode($user));
//
//        $body = $response->getBody();
//        self::assertEquals(500, $response->getStatusCode());
//        self::assertEquals("L'utilisateur existe déjà", json_decode($body));
//    }
//
//    public function testNoEmail() {
//        $user = [
//            "email" => '',
//            "password" => 'azerty123',
//            "firstname" => 'Valentin',
//            "lastname" => 'Roussel',
//            "birthday" => new \DateTime('1997-05-15'),
//        ];
//
//        $client = new Client();
//        $response = $client->request("POST", self::$url."/login", $user);
//
////        $body = $response->getBody();
//        self::assertEquals(500, $response->getStatusCode());
//  //      self::assertEquals("Il manque l'email", json_decode($body));
//    }
//}
